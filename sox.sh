#!/usr/bin/bash
set -e

sudo apt install -y ninja-build autoconf autogen automake build-essential libasound2-dev autoconf-archive python libtool pkg-config docbook2x git wget subversion gettext libtool-bin cmake\
	#libogg-dev libvorbis-dev\
    

wavpack="5.6.0"
amr="0.1.6"
twolame="0.4.0"
ogg="1.3.5"
vorbis="1.3.7"
opus="1.4"
flac="1.4.2"
libpng="1.6.39"
opusfile="0.12"
libsndfile="1.2.0"
openssl="OpenSSL_1_1_1t"
lame="RELEASE__3_100"
zlib="1.2.13"

#arch='x86_64-w64' mm='' ldf='' ossl='mingw64'
arch='i686-w64' mm='-m32' ldf='-m32 -L/usr/i686-w64-mingw32/lib' ossl='mingw'

mkdir ~/sox

cd ~/sox
git clone -b $wavpack --depth 1 https://github.com/dbry/WavPack
cd WavPack*
./autogen.sh
cp configure configure32
sed -i -e 's/OPT_ASM_X64/OPT_ASM_X86/g' -e 's/asm_selected=x64/asm_selected=x86/g' configure32
if [[ $mm == -m32 ]]
then
RC="x86_64-w64-mingw32-windres -F pe-i386" PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure32 LDFLAGS="$ldf" CFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe" \
    --prefix=/usr/$arch-mingw32 --disable-shared \
    --enable-silent-rules --host=x86_64-w64-mingw32
else
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure LDFLAGS="$ldf" CFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe" \
    --prefix=/usr/$arch-mingw32 --disable-shared \
    --enable-silent-rules --host=x86_64-w64-mingw32
fi
make -j$(nproc)
sudo make install

cd ~/sox
wget -c https://sourceforge.net/projects/opencore-amr/files/opencore-amr/opencore-amr-$amr.tar.gz -O - | tar xfvz -
cd opencore-amr*
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared LDFLAGS="$ldf" CXXFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe" \
    --host=x86_64-w64-mingw32
make -j$(nproc)
sudo make install

cd ~/sox
git clone -b $twolame --depth 1 https://github.com/njh/twolame
cd twolame*
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-twolame/0001-mingw32-does-not-need-handholding.all.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-twolame/0005-silent.mingw.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-twolame/0002-Add-missing-TL_API.patch
patch -p1 < 0001-mingw32-does-not-need-handholding.all.patch
patch -p1 < 0005-silent.mingw.patch
patch -p1 < 0002-Add-missing-TL_API.patch
autoreconf -fiv
touch doc/twolame.1
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure LDFLAGS="$ldf" CFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe" \
    --prefix=/usr/$arch-mingw32 --disable-shared --enable-silent-rules \
    --host=x86_64-w64-mingw32
make -j$(nproc)
rm twolame.spec
sudo make install

cd ~/sox
wget https://ftp.osuosl.org/pub/xiph/releases/ogg/libogg-$ogg.tar.gz -O - | tar -xzvf -
cd libogg*
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared LDFLAGS="$ldf" CFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe" \
    --enable-silent-rules --host=x86_64-w64-mingw32
make -j$(nproc)
rm libogg.spec
sudo make install

cd ~/sox
#wget 'https://git.xiph.org/?p=vorbis.git;a=snapshot;h=HEAD;sf=tgz' -O - | tar -xzvf -
wget https://ftp.osuosl.org/pub/xiph/releases/vorbis/libvorbis-$vorbis.tar.gz -O - | tar -xzvf -
cd libvorbis*
autoreconf -I m4 -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-oggtest LDFLAGS="$ldf" CFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe" \
    --disable-shared --enable-silent-rules --host=x86_64-w64-mingw32 \
    --target=$arch-mingw32
make -j$(nproc)
rm libvorbis.spec
sudo make install

cd ~/sox
#wget https://archive.mozilla.org/pub/opus/opus-$opus.tar.gz -O - | tar -xzvf -
wget https://ftp.osuosl.org/pub/xiph/releases/opus/opus-$opus.tar.gz -O - | tar -xzvf -
cd opus*
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-doc LDFLAGS="$ldf" CFLAGS="-D_FORTIFY_SOURCE=0 $mm -march=core2 -mtune=native -O2 -s -pipe" \
    --disable-shared --enable-custom-modes --enable-ambisonics \
    --host=x86_64-w64-mingw32
make -j$(nproc)
sudo make install

cd ~/sox
svn checkout https://svn.code.sf.net/p/lame/svn/tags/$lame/lame
cd lame
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-lame/0002-07-field-width-fix.all.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-lame/0005-no-gtk.all.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-lame/0006-dont-use-outdated-symbol-list.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-lame/0008-skip-termcap.patch
patch -p1 < 0002-07-field-width-fix.all.patch
patch -p1 < 0005-no-gtk.all.patch
patch -p1 < 0006-dont-use-outdated-symbol-list.patch
patch -p1 < 0008-skip-termcap.patch
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared LDFLAGS="$ldf" CFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe" LDFLAGS="$ldf" CXXFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe" \
    --enable-expopt=full --with-fileio=lame --enable-nasm \
    --enable-silent-rules --host=x86_64-w64-mingw32
make -j$(nproc)
rm lame.spec
sudo make install

cd ~/sox
wget https://ftp.osuosl.org/pub/xiph/releases/flac/flac-$flac.tar.xz -O - | tar -xJvf -
cd flac*
#./autogen.sh
#PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared --disable-xmms-plugin --disable-doxygen-docs --host=x86_64-w64-mingw32 LDFLAGS="$ldf" CFLAGS="-D_FORTIFY_SOURCE=0 $mm -march=core2 -mtune=native -O2 -s -pipe" LDFLAGS="$ldf" CXXFLAGS="-D_FORTIFY_SOURCE=0 $mm -march=core2 -mtune=native -O2 -s -pipe"
#make -j$(nproc)
#sudo make install
cmake -B $arch -G Ninja -DCMAKE_BUILD_TYPE=Release -DBUILD_CXXLIBS=OFF -DBUILD_PROGRAMS=OFF -DBUILD_EXAMPLES=OFF -DBUILD_TESTING=OFF -DBUILD_DOCS=OFF -DCMAKE_PREFIX_PATH=/usr/$arch-mingw32 -DCMAKE_INSTALL_PREFIX=/usr/$arch-mingw32 -DCMAKE_C_FLAGS_RELEASE="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static"
cmake --build $arch
cmake --install $arch

cd ~/sox
wget https://zlib.net/zlib-$zlib.tar.gz -O - | tar xfvz -
cd zlib-*
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-zlib/04-fix-largefile-support.patch
patch -p1 < 04-fix-largefile-support.patch
CFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe" LDFLAGS="$ldf" CC=x86_64-w64-mingw32-gcc AR=x86_64-w64-mingw32-ar RANLIB=x86_64-w64-mingw32-ranlib ./configure --prefix=/usr/$arch-mingw32 --static
make -j$(nproc)
sudo make install

cd ~/sox
wget -c https://sourceforge.net/projects/libpng/files/libpng16/$libpng/libpng-$libpng.tar.xz -O - | tar -xJvf -
cd libpng*
cmake -G "Ninja" -DCMAKE_INSTALL_PREFIX=/usr/$arch-mingw32 \
    -DCMAKE_TOOLCHAIN_FILE="/usr/x86_64-w64-mingw32/toolchain-x86_64-w64-mingw32.cmake" \
-DCMAKE_C_FLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe" -DPNG_SHARED:bool=off -DPNG_TESTS:bool=off \
-DZLIB_INCLUDE_DIR=/usr/$arch-mingw32/include \
    -DZLIB_LIBRARY=/usr/$arch-mingw32/lib/libz.a
ninja
sudo ninja install

cd ~/sox
wget -c https://downloads.sourceforge.net/mad/libmad-0.15.1b.tar.gz -O - | tar -xzvf -
cd libmad*
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0001-no-undefined.mingw.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0003-update-ac-and-silent-rules.mingw.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0004-amd64-64bit.diff
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0006-optimize.diff
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0007-libmad.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0008-md_size.diff
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0009-length-check.patch
patch -p1 < 0001-no-undefined.mingw.patch
patch -p1 < 0003-update-ac-and-silent-rules.mingw.patch
patch -p1 < 0004-amd64-64bit.diff
patch -p1 < 0006-optimize.diff
patch -p1 < 0007-libmad.patch
patch -p1 < 0008-md_size.diff
patch -p1 < 0009-length-check.patch
touch NEWS AUTHORS ChangeLog
rm aclocal.m4
rm Makefile.in
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared --host=x86_64-w64-mingw32 LDFLAGS="$ldf" CFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe"
make -j$(nproc)
sudo make install

#cd ~/sox
#git clone -b $openssl --depth 1 https://github.com/openssl/openssl
#cd openssl
#PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./Configure --prefix=/usr/$arch-mingw32 --cross-compile-prefix=x86_64-w64-mingw32- $ossl --release LDFLAGS="$ldf" CFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe"
#make -j$(nproc)
#sudo make install

sudo sed -i 's/unknown/1.4/g' /usr/$arch-mingw32/lib/pkgconfig/opus.pc

cd ~/sox
wget https://ftp.osuosl.org/pub/xiph/releases/opus/opusfile-$opusfile.tar.gz -O - | tar -xzvf -
cd opusfile*
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-opusfile/no-openssl-wincert.patch
patch -p1 < no-openssl-wincert.patch
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 LDFLAGS="$ldf" CFLAGS="$mm -march=core2 -mtune=native -O2 -s -pipe" --disable-shared --host=x86_64-w64-mingw32 --disable-http
make -j$(nproc)
sudo make install

cd ~/sox
git clone -b $libsndfile --depth 1 https://github.com/erikd/libsndfile.git
cd libsndfile
#autoreconf -fiv
#PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared LDFLAGS="$ldf" CFLAGS="-D_FORTIFY_SOURCE=0 $mm -march=core2 -mtune=native -O2 -s -pipe" \
#    --disable-alsa --disable-external-libs --disable-sqlite \
#    --host=x86_64-w64-mingw32
#make -j$(nproc)
#rm libsndfile.spec
#sudo make install
cmake -B $arch -G Ninja -DCMAKE_BUILD_TYPE=Release -DBUILD_PROGRAMS=OFF -DBUILD_EXAMPLES=OFF -DBUILD_TESTING=OFF -DCMAKE_C_FLAGS_RELEASE="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" -DCMAKE_INSTALL_PREFIX=/usr/$arch-mingw32 -DENABLE_EXTERNAL_LIBS=OFF
cmake --build $arch
sudo cmake --install $arch

cd ~/sox
git clone -b master --depth 1 https://git.code.sf.net/p/sox/code sox
cd sox
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --host=x86_64-w64-mingw32 --disable-shared LDFLAGS="$ldf" CFLAGS="-DFLAC__NO_DLL $mm -march=core2 -mtune=native -O2 -s -pipe"
make -j$(nproc)