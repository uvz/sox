#!/usr/bin/bash
set -e

wavpack="5.6.0"
amr="0.1.6"
twolame="0.4.0"
ogg="1.3.5"
vorbis="1.3.7"
opus="1.4"
flac="1.4.2"
libpng="1.6.39"
opusfile="0.12"
libsndfile=" 1.2.0"
#openssl="OpenSSL_1_1_1n"
lame="RELEASE__3_100"
sox="master"
zlib="1.2.13"

arch='x86_64-w64' mm=''
#arch='i686-w64' mm='-m32'

mkdir ~/sox

cd ~/sox
git clone -b $wavpack --depth 1 https://github.com/dbry/WavPack
cd WavPack*
./autogen.sh
cp configure configure32
sed -i -e 's/OPT_ASM_X64/OPT_ASM_X86/g' -e 's/asm_selected=x64/asm_selected=x86/g' configure32
if [[ $mm == -m32 ]]
then
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure32 CFLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" \
    --prefix=/usr/$arch-mingw32 --disable-shared \
    --enable-silent-rules
else
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure CFLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" \
    --prefix=/usr/$arch-mingw32 --disable-shared \
    --enable-silent-rules
fi
make -j $(nproc)
make install

cd ~/sox
wget -c https://sourceforge.net/projects/opencore-amr/files/opencore-amr/opencore-amr-$amr.tar.gz -O - | tar xfvz -
cd opencore-amr*
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared CFLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static"
make -j $(nproc)
make install

cd ~/sox
git clone -b $twolame --depth 1 https://github.com/njh/twolame
cd twolame*
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-twolame/0001-mingw32-does-not-need-handholding.all.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-twolame/0005-silent.mingw.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-twolame/0002-Add-missing-TL_API.patch
patch -p1 < 0001-mingw32-does-not-need-handholding.all.patch
patch -p1 < 0005-silent.mingw.patch
patch -p1 < 0002-Add-missing-TL_API.patch
autoreconf -fiv
touch doc/twolame.1
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure CFLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" \
    --prefix=/usr/$arch-mingw32 --disable-shared --enable-silent-rules
make -j $(nproc)
rm twolame.spec
make install

cd ~/sox
wget https://ftp.osuosl.org/pub/xiph/releases/ogg/libogg-$ogg.tar.gz -O - | tar -xzvf -
cd libogg*
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared CFLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" \
    --enable-silent-rules
make -j $(nproc)
rm libogg.spec
make install

cd ~/sox
#wget 'https://git.xiph.org/?p=vorbis.git;a=snapshot;h=HEAD;sf=tgz' -O - | tar -xzvf -
wget https://ftp.osuosl.org/pub/xiph/releases/vorbis/libvorbis-$vorbis.tar.gz -O - | tar -xzvf -
cd libvorbis*
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 CFLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" \
    --disable-shared --enable-silent-rules
make -j$(nproc)
#rm libvorbis.spec
make install

cd ~/sox
#wget https://archive.mozilla.org/pub/opus/opus-$opus.tar.gz -O - | tar -xzvf -
wget https://ftp.osuosl.org/pub/xiph/releases/opus/opus-$opus.tar.gz -O - | tar -xzvf -
cd opus*
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-doc CFLAGS="-D_FORTIFY_SOURCE=0 -march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" \
--disable-shared --enable-custom-modes
make -j $(nproc)
make install

cd ~/sox
svn checkout https://svn.code.sf.net/p/lame/svn/tags/$lame/lame
cd lame
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-lame/0002-07-field-width-fix.all.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-lame/0005-no-gtk.all.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-lame/0006-dont-use-outdated-symbol-list.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-lame/0008-skip-termcap.patch
patch -p1 < 0002-07-field-width-fix.all.patch
patch -p1 < 0005-no-gtk.all.patch
patch -p1 < 0006-dont-use-outdated-symbol-list.patch
patch -p1 < 0008-skip-termcap.patch
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared CFLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" CXXFLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" \
    --enable-nasm \
    --enable-silent-rules --disable-frontend
make -j $(nproc)
rm lame.spec
make install

cd ~/sox
wget https://ftp.osuosl.org/pub/xiph/releases/flac/flac-$flac.tar.xz -O - | tar -xJvf -
cd flac*
#./autogen.sh
#PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared --disable-xmms-plugin --disable-doxygen-docs CFLAGS="-D_FORTIFY_SOURCE=0 -march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" CXXFLAGS="-D_FORTIFY_SOURCE=0 -march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static"
#make -j $(nproc)
#make install
cmake -B $arch -G Ninja -DCMAKE_BUILD_TYPE=Release -DBUILD_CXXLIBS=OFF -DBUILD_PROGRAMS=OFF -DBUILD_EXAMPLES=OFF -DBUILD_TESTING=OFF -DBUILD_DOCS=OFF -DCMAKE_PREFIX_PATH=/usr/$arch-mingw32 -DCMAKE_INSTALL_PREFIX=/usr/$arch-mingw32 -DCMAKE_C_FLAGS_RELEASE="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static"
cmake --build $arch
cmake --install $arch

cd ~/sox
wget https://zlib.net/zlib-$zlib.tar.gz -O - | tar xfvz -
cd zlib-*
sed -i 's/share\/pkgconfig/lib\/pkgconfig/g' CMakeLists.txt
sed -i 's/add_library(zlibstatic STATIC/add_library(z STATIC/g' CMakeLists.txt
sed -i 's/TARGETS zlib zlibstatic/TARGETS zlib z/g' CMakeLists.txt
mkdir build44
cd build44
cmake -G Ninja -DCMAKE_C_FLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" -DCMAKE_INSTALL_PREFIX=/usr/$arch-mingw32 ..
ninja
ninja install

cd ~/sox
wget -c https://sourceforge.net/projects/libpng/files/libpng16/$libpng/libpng-$libpng.tar.xz -O - | tar -xJvf -
cd libpng*
cmake -G "Ninja" -DCMAKE_INSTALL_PREFIX=/usr/$arch-mingw32 \
    -DCMAKE_C_FLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" -DPNG_SHARED:bool=off -DPNG_TESTS:bool=off \
-DZLIB_INCLUDE_DIR=/usr/$arch-mingw32/include \
    -DZLIB_LIBRARY=/usr/$arch-mingw32/lib/libz.a
ninja
ninja install

cd ~/sox
wget -c https://downloads.sourceforge.net/mad/libmad-0.15.1b.tar.gz -O - | tar -xzvf -
cd libmad*
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0001-no-undefined.mingw.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0003-update-ac-and-silent-rules.mingw.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0004-amd64-64bit.diff
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0006-optimize.diff
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0007-libmad.patch
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0008-md_size.diff
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libmad/0009-length-check.patch
patch -p1 < 0001-no-undefined.mingw.patch
patch -p1 < 0003-update-ac-and-silent-rules.mingw.patch
patch -p1 < 0004-amd64-64bit.diff
patch -p1 < 0006-optimize.diff
patch -p1 < 0007-libmad.patch
patch -p1 < 0008-md_size.diff
patch -p1 < 0009-length-check.patch
touch NEWS AUTHORS ChangeLog
rm aclocal.m4
rm Makefile.in
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared CFLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static"
make -j $(nproc)
make install

cd ~/sox
wget https://ftp.osuosl.org/pub/xiph/releases/opus/opusfile-$opusfile.tar.gz -O - | tar -xzvf -
cd opusfile*
wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-opusfile/no-openssl-wincert.patch
patch -p1 < no-openssl-wincert.patch
autoreconf -fiv
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 CFLAGS="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" --disable-shared --disable-http
make -j $(nproc)
make install

cd ~/sox
git clone -b $libsndfile --depth 1 https://github.com/erikd/libsndfile.git
cd libsndfile
#autoreconf -fiv
#PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared CFLAGS="-D_FORTIFY_SOURCE=0 -march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" \
#    --disable-alsa --disable-external-libs --disable-sqlite
#make -j $(nproc)
#make install
cmake -B $arch -G Ninja -DCMAKE_BUILD_TYPE=Release -DBUILD_PROGRAMS=OFF -DBUILD_EXAMPLES=OFF -DBUILD_TESTING=OFF -DCMAKE_C_FLAGS_RELEASE="-march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static" -DCMAKE_INSTALL_PREFIX=/usr/$arch-mingw32 -DENABLE_EXTERNAL_LIBS=OFF
cmake --build $arch
cmake --install $arch

cd ~/sox
git clone -b $sox --depth 1 https://github.com/mansr/sox
cd sox
autoreconf -fiv # autoconf-archive needed
PKG_CONFIG_PATH=/usr/$arch-mingw32/lib/pkgconfig ./configure --prefix=/usr/$arch-mingw32 --disable-shared CFLAGS="-DFLAC__NO_DLL -march=core2 -mtune=native -O2 -s -pipe -static-libgcc -static-libstdc++ -static -I/usr/$arch-mingw32/include" --disable-openmp LDFLAGS="-L/usr/$arch-mingw32/lib"
make -j $(nproc)
make install
